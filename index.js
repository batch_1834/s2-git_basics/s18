console.log("Hello world!")

let trainer ={};

	trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},
	talk: function(){
		console.log("Pikachu! I choose you!");
	}
	
}
console.log(trainer);
console.log("The Result of dot notation:");
console.log(trainer.name);
console.log("Result of square bracket:");
console.log(trainer["pokemon"]);
console.log("Result of talk method:");
trainer.talk();

let myPokemon = {
	name: "Pikachu",
	level: 12,
	health: 24,
	attack: 12,
	tackle: function(){
		console.log("This pokemon tackled targetpokemon");
		console.log("targetPokemon's health  is reduced to newTargetPokemonHealth");
	},
	faint: function(){
		console.log("Pokemon fainted");
	}
}
function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = level * 2;
	this.attack = level;

	this.tackle = function(target){
		console.log(this.name+" tackled "+target.name);
		target.health = target.health - this.attack;
		if (target.health >= 0) {
				console.log(target.name+"'s health is now reduced to "+target.health);
				this.faint();
				

			}
		else{
			console.log(target.name+"'s health is now reduced to "+target.health)
			console.log(target.name+" fainted");
		}
		

		
	}	
	this.faint = function(){
		console.log(this.name+" fainted");
	}
}
let pikachu = new Pokemon("Pikachu", 12)
console.log(pikachu);

let geodude = new Pokemon("Geodude", 8)
console.log(geodude);

let mewtwo = new Pokemon("Mewtwo", 100)
console.log(mewtwo);



geodude.tackle(pikachu)

mewtwo.tackle(geodude)

